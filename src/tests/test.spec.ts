import { mount } from '@vue/test-utils';
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest';
import { ref } from 'vue';
import { vInfiniteScroll } from '../infinite-scroll-plugin/directives/v-infinite-scroll'; // Update the path accordingly

describe('vInfiniteScroll directive', () => {
  let wrapper;

  beforeEach(() => {
    // Create a mock for IntersectionObserver
    global.IntersectionObserver = class IntersectionObserver {
      observe() {}
      disconnect() {}
    };

    // Mock the window object
    global.window = Object.create(window);
    Object.defineProperty(window, 'addEventListener', { value: vi.fn() });
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it('should call the binding value when disabled attribute changes', async () => {
    const Component = {
      template: `
        <div v-infinite-scroll="onInfiniteScroll" :data-infinite-scroll-disabled="disabled">
          <div v-for="item in items" :key="item">{{ item }}</div>
        </div>
      `,
      directives: {
        infiniteScroll: vInfiniteScroll,
      },
      setup() {
        const disabled = ref(false);
        const items = Array.from({ length: 20 }, (_, i) => `Item ${i + 1}`);

        const onInfiniteScroll = vi.fn();

        return { disabled, items, onInfiniteScroll };
      },
    };

    wrapper = mount(Component);

    // Change the disabled attribute
    wrapper.vm.disabled = true;

    // Wait for next tick to allow the directive's logic to run
    await wrapper.vm.$nextTick();

    // Ensure that the binding value has not been called
    expect(wrapper.vm.onInfiniteScroll).not.toHaveBeenCalled();

    // Change the disabled attribute back to false
    wrapper.vm.disabled = false;

    // Wait for next tick to allow the directive's logic to run
    await wrapper.vm.$nextTick();

    // Ensure that the binding value has been called
    expect(wrapper.vm.onInfiniteScroll).toHaveBeenCalled();
  });
  it('should not call the binding value when disabled is true', async () => {
    const Component = {
      template: `
        <div v-infinite-scroll="onInfiniteScroll" :data-infinite-scroll-disabled="true">
          <div v-for="item in items" :key="item">{{ item }}</div>
        </div>
      `,
      directives: {
        infiniteScroll: vInfiniteScroll,
      },
      setup() {
        const items = ref(Array.from({ length: 20 }, (_, i) => `Item ${i + 1}`));
        const onInfiniteScroll = vi.fn();

        return { items, onInfiniteScroll };
      },
    };

    wrapper = mount(Component);

    // Simulate scrolling to the bottom
    wrapper.element.scrollTop = wrapper.element.scrollHeight;

    // Trigger a scroll event
    wrapper.trigger('scroll');

    // Wait for next tick to allow the directive's logic to run
    await wrapper.vm.$nextTick();

    // Ensure that the binding value has not been called
    expect(wrapper.vm.onInfiniteScroll).not.toHaveBeenCalled();
  });
});
