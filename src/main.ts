import { createApp } from 'vue'
import App from './App.vue'
import { infiniteScrollPlugin } from './infinite-scroll-plugin'

createApp(App).use(infiniteScrollPlugin).mount('#app')
