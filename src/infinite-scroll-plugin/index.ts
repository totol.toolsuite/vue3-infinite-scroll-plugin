import type { App } from "vue";
import { vInfiniteScroll } from "./directives/v-infinite-scroll";
export * from './types';

export function infiniteScrollPlugin(app: App) {  
  app.directive('infinite-scroll', vInfiniteScroll)
}