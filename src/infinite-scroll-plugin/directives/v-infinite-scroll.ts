import { DirectiveBinding } from 'vue';
import type { ScrollableHtmlElement } from '../types';

export const vInfiniteScroll = {
  mounted(el: ScrollableHtmlElement, binding: DirectiveBinding) {
    let isThrottled = false;
    let disabled = false

    const throttleTime = Number(el.getAttribute('data-infinite-scroll-delay')) || 2000;

    const detectionMargin = Number(el.getAttribute('data-infinite-scroll-margin')) || 0;

    const checkDisabled = () => {
      const disabledAttribute = el.getAttribute('data-infinite-scroll-disabled');
      disabled = disabledAttribute !== null && disabledAttribute.toLowerCase() === 'true';
    }

    const checkBottomVisibility = () => {
      checkDisabled()
      const rect = el.getBoundingClientRect();
      const isBottomInView = rect.bottom <= window.innerHeight + detectionMargin;

      if (isBottomInView && !isThrottled && !disabled) {
        binding.value();
        isThrottled = true;

        // Throttle the function execution
        setTimeout(() => {
          isThrottled = false;
          checkBottomVisibility(); // Recursive call to continuously check bottom visibility
        }, throttleTime);
      }
    };

    const observer = new IntersectionObserver((entries) => {
      entries.forEach(() => {
        checkBottomVisibility();
      });
    });

    const disabledAttributeChange = () => {
      const disabledAttribute = el.getAttribute('data-infinite-scroll-disabled');
      disabled = disabledAttribute !== null && disabledAttribute.toLowerCase() === 'true';
      checkBottomVisibility();
    };

    // Observe changes in the data-infinite-scroll-disabled attribute
    const disabledObserver = new MutationObserver(disabledAttributeChange);
    disabledObserver.observe(el, { attributes: true, attributeFilter: ['data-infinite-scroll-disabled'] });

    
    const onScroll = () => {
      checkBottomVisibility();
    };

    // Start observing the target element
    observer.observe(el);

    // Listen for scroll events to check bottom visibility after scrolling
    window.addEventListener('scroll', onScroll);

    // Cleanup the observer and event listener when the component is destroyed
    const destroyObserver = () => {
      observer.disconnect();
      window.removeEventListener('scroll', onScroll);
    };
    el.__v_destroyInfiniteScrollObserver = destroyObserver;
  },

  beforeUnmount(el: HTMLElement & { __v_destroyInfiniteScrollObserver?: Function }) {
    // Disconnect the observer and remove the event listener when the component is destroyed
    if (el.__v_destroyInfiniteScrollObserver) {
      el.__v_destroyInfiniteScrollObserver();
      delete el.__v_destroyInfiniteScrollObserver;
    }
  }
};
