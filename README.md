# Vue 3 Infinite Scroll Plugin

A Vue 3 directive for implementing infinite scroll functionality.

## Test it by yourself

Test the directive by yourself [in this sandbox](https://codesandbox.io/p/devbox/vue-3-infinite-scroll-directive-plugin-fff9ld)

![Demo](https://i.imgur.com/TlsY876.gif)

## Installation

```bash
npm install npm i vue-3-infinite-scroll-directive-plugin
yarn add npm i vue-3-infinite-scroll-directive-plugin
```

## Usage
Import and Register the Plugin
In your main application file (e.g., main.ts or main.js), import and register the plugin:

```typescript
import { createApp } from 'vue';
import App from './App.vue';
import { infiniteScrollPlugin } from 'vue-3-infinite-scroll-directive-plugin';

createApp(App).use(infiniteScrollPlugin).mount('#app');
```

## Directive Usage
Add the v-infinite-scroll directive to the scrollable element in your template. Specify the handler function to be called when the bottom of the element is reached.

```vue
<template>
  <div v-infinite-scroll="handleInfiniteScroll" 
    data-infinite-scroll-margin="20" 
    data-infinite-scroll-delay="200" 
    :data-infinite-scroll-disabled="isLoading">
    <!-- Your content here -->
  </div>
  <div class="loader-wrapper" v-if="isLoading">
    <span class="loader"></span>
  </div>
</template>
``` 

## Example Setup

```Vue
<script lang="ts" setup>
  import { ref } from 'vue';

  const todos = ref([]);
  const nextPage = ref(1);
  const isLoading = ref(false);

  const handleInfiniteScroll = async () => {
    isLoading.value = true;

    const data = await (await fetch(`https://jsonplaceholder.typicode.com/todos/${nextPage.value}`)).json();
    todos.value.push(data);
    nextPage.value++;

    setTimeout(() => {
      isLoading.value = false;
    }, Math.random() * 2000);
  };
</script>
<template>
  <div v-infinite-scroll="handleInfiniteScroll" 
    data-infinite-scroll-margin="20" 
    data-infinite-scroll-delay="200" 
    :data-infinite-scroll-disabled="isLoading"
  >
    <ul>
      <li  v-for="todo in todos" :key="todo.id"  class="todo">
        <div>
          {{ todo.title }}
        </div>
      </li>
    </ul>
  </div>
  <div class="loader-wrapper" v-if="isLoading">
    <span class="loader"></span>
  </div>
</template>
```
## Options

- v-infinite-scroll: Pass the callback to execute when bottom is reached

- data-infinite-scroll-margin: Set the margin (in pixels) from the bottom of the scrollable element to trigger the infinite scroll. Default is 0.

- data-infinite-scroll-delay: Set the delay (in milliseconds) before allowing another infinite scroll trigger. Default is 2000.

- data-infinite-scroll-disabled: Bind this attribute to a variable to enable/disable infinite scroll based on its truthiness.

## Types
```typescript
export type ScrollableHtmlElement = HTMLElement & { __v_destroyInfiniteScrollObserver?: Function };
```

## Cleanup
The plugin takes care of disconnecting the observer and removing the event listener when the component is destroyed.

## License
This project is licensed under the MIT License - see the LICENSE file for details.
